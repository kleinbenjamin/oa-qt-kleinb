#include <iostream>
#include <QDirIterator>
#include <QRegExp>
#include <QString>
#include <QFileInfo>
#include <QDateTime>

int main(int argc, char **argv)
{

  QRegExp re("\\d*");
  QString amount = argv[2];

    if (argc < 3) {
        std::cout << "Insufficient parameters given\n";
        return 1;
    } else if (!re.exactMatch(amount) || amount.toInt() < 0) {
      std::cout << "Invalid number given\n";
      return 1;
    }


    QDirIterator dirIterator(
        argv[1],
        QDir::NoDotAndDotDot | QDir::AllEntries,
        QDirIterator::Subdirectories
    );

    int n = amount.toInt();
    int i = 0;
    while (dirIterator.hasNext() && i < n) {
        QFileInfo fileinfo(dirIterator.next());
        if(fileinfo.isFile()) {
          QDateTime born = fileinfo.birthTime();
          QString st = born.toString("yyyy-MM-dd HH:mm");
          std::cout << "\t" << fileinfo.size() << " kb\t" << st.toStdString() << "\t\t" << dirIterator.fileName().toStdString() << std::endl;
        } else {
          std::cout << "Directory: " << fileinfo.path().toStdString() << std::endl;
        }
        ++i;
    }

    return 0;
}
